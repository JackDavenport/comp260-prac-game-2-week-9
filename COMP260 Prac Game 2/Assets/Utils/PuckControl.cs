﻿
using UnityEngine;
using System.Collections;
[RequireComponent(typeof(AudioSource))]

public class PuckControl : MonoBehaviour {
	public AudioClip wallCollideClip;
	public AudioClip paddleCollideClip;
	public LayerMask paddleLayer;
	private AudioSource audio;
	public Transform startingPos;
	private Rigidbody rigidbody;
	void Start(){
		audio = GetComponent<AudioSource> ();
		rigidbody = GetComponent<Rigidbody> ();
		ResetPosition ();
	}
	public void ResetPosition(){
		rigidbody.MovePosition(startingPos.position);
		rigidbody.velocity = Vector3.zero;
	}

	void OnCollisionEnter(Collision collision){
		if (paddleLayer.Contains (collision.gameObject)) {
			audio.PlayOneShot (paddleCollideClip);
		} else {
			audio.PlayOneShot (wallCollideClip);
		}
	}
}
