﻿using UnityEngine;
using System.Collections;
[RequireComponent(typeof(AudioSource))]




public class Goal : MonoBehaviour {
	public AudioClip scoreClip;
	private AudioSource audio;
	public delegate void ScoreGoalHandler(int player);
	public ScoreGoalHandler scoreGoalEvent;
	public int player;
	// Use this for initialization
	void Start () {
		audio = GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void OnTriggerEnter (Collider collider) {
		audio.PlayOneShot (scoreClip);
		PuckControl puck = collider.gameObject.GetComponent<PuckControl> ();
		puck.ResetPosition ();
		if (scoreGoalEvent != null) {
			scoreGoalEvent (player);
		}
	}
}
