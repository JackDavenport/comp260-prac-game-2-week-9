﻿using UnityEngine;
using System.Collections;
[RequireComponent(typeof(Rigidbody))]

public class AI : MonoBehaviour {
	private Rigidbody rigidbody;
	public float speed = 5.0f;
	void Start () {
		rigidbody = GetComponent<Rigidbody> ();
		rigidbody.useGravity = false;
	}
	public Transform target;
	void FixedUpdate(){
		Vector3 direction = target.position - transform.position;
		direction = direction.normalized;

		Vector3 velocity = direction * speed;
		rigidbody.velocity = velocity;
	}
	// Use this for initialization

	// Update is called once per frame
	void Update () {
	
	}
}
